create database submissao;
use submissao;

create table artigo(
titulo varchar(40),
situacao varchar(40),
autor varchar(40),
instituicao varchar(40),
pChave varchar(40),
resumo varchar(40),
abstracT varchar(40),
primary key(titulo)
)default charset = utf8;

create table resumo(
titulo varchar(40),
situacao varchar(40),
autor varchar(40),
instituicao varchar(40), 
p_chave varchar(40),
primary key (titulo)
)default charset = utf8;

create table palestra(
titulo varchar(40),
situacao varchar(40),
autor varchar(40),
instituicao varchar(40), 
p_chave varchar(40),
resumo varchar(40),
abstracT varchar(40),
duracao varchar(3),
curriculo varchar(100),
primary key (titulo)
)default charset = utf8;

create table miniCurso(
titulo varchar(40),
situacao varchar(40),
autor varchar(40),
resumo varchar(40),
abstracT varchar(40),
duracao varchar(3),
curriculo varchar(100),
recursos varchar(40),
metodologia varchar(100),
primary key (titulo)
)default charset = utf8;

create table monografia(
titulo varchar(40),
situacao varchar(40),
tipo varchar(40),
autor varchar(40), 
instituicao varchar(40),
orientador varchar(40), 
curso varchar(40), 
ano varchar(4),
nPaginas varchar(3),
p_chave varchar(40),
resumo varchar(40),
abstracT varchar(40),
primary key (titulo)
)default charset = utf8;

create table relatorioTecnico(
titulo varchar(40),
situacao varchar(40),
autor varchar(40), 
instituicao varchar(40), 
ano varchar(4),
nPaginas varchar(3),
p_chave varchar(40),
resumo varchar(40),
abstracT varchar(40),
primary key (titulo)
)default charset = utf8;

